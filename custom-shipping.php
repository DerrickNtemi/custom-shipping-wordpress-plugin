<?php
/*
Plugin Name: Derrick's custom shipping - Rate
Plugin URI: 
Description: Adds new shipping method. 
Version: 1.0.0
Author: Derrick kimathi
Author URI: http://thisisderrick.com
*/
 
/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
  function your_shipping_method_init() {
    if ( ! class_exists( 'WC_Your_Shipping_Method' ) ) {
      class WC_Your_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
          $this->id                 = 'flat_rate_reg'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'Our Shipping Price' );  // Title shown in admin
          $this->method_description = __( '15,000 ugx for every 1-24 bible(15,000 ugx per carton)' ); // Description shown in admin
 
          $this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
          $this->title              = "Bible Society shipping rate"; // This can be added as an setting but for this example its forced.
 
          $this->init();
        }
 
        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.
 
          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }
 
        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package ) {
          
          // We will add the cost, rate and logics in here
                    $cartons = 0;
                    $cost = 0;
                    $number = 24;
                    $set_price = 15000;
                    $cal_carton = 0;
                     $total_items = 0;
          
          foreach($package["contents"] as $item_k => $item)
          {
              
           $total_items += $item['quantity'];   
             $cal_carton =  $total_items/$number;
             $cartons = ceil( $cal_carton);
          }
          
          $cost = ($set_price * $cartons);
          
          $rate = array(
            'id' => $this->id,
            'label' => $this->title,
            'cost' => $cost,
            
          );
 
          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }
  }
 
  add_action( 'woocommerce_shipping_init', 'your_shipping_method_init' );
 
  function add_your_shipping_method( $methods ) {
    $methods[] = 'WC_Your_Shipping_Method';
    return $methods;
  }
 
  add_filter( 'woocommerce_shipping_methods', 'add_your_shipping_method' );
}